import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

import { take, map } from 'rxjs/operators';
import { Reporte } from '../models/reporte.interface';
import { AngularFireDatabase } from '@angular/fire/database';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { User } from '../models/user.interface';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  myForm: FormGroup;
  private reporte: Reporte;
  private usuario: User;


  constructor(public navCtrl: NavController,
    private formBuilder: FormBuilder, private authService: AuthService, 
    private afDB: AngularFireDatabase, private alertController: AlertController
    ) {
      this.myForm = this.createMyForm();
  }

  ngOnInit() {
    this.authService.getUserAuth().subscribe(user => {
      this.usuario = user;
    });
  }

  private createMyForm() {
    return this.formBuilder.group({
      fecha: ['', Validators.required],
      hora: ['', Validators.required],
      nombre: ['', Validators.required],
      codigo: ['', Validators.required],
      edad: ['', Validators.required],
      transporte: ['', Validators.required],
      cargo: ['', Validators.required],
      tos: ['', Validators.required],
      fatiga: ['', Validators.required],
      fiebre: ['', Validators.required],
      resfriado: ['', Validators.required],
      escalofrios: ['', Validators.required],
      dolorMuscular: ['', Validators.required],
      dolorCabeza: ['', Validators.required],
      dolorGarganta: ['', Validators.required],
      perdidaOlfato: ['', Validators.required],
      contactoCovid: ['', Validators.required],
      contactoSintomas: ['', Validators.required],
      viaje: ['', Validators.required],
    });
  }

  saveReport() {
    this.reporte = {
      uid: '0',
      fecha: this.myForm.controls['fecha'].value,
      hora: this.myForm.controls['hora'].value,
      user: {
        uid: this.usuario.uid,
        email: this.usuario.email,
        displayName: this.usuario.displayName,
        emailVerified: this.usuario.emailVerified
      },
      nombre: this.myForm.controls['nombre'].value,
      codigo: this.myForm.controls['codigo'].value,
      edad: this.myForm.controls['edad'].value,
      medioTransporte: this.myForm.controls['transporte'].value,
      cargo: this.myForm.controls['cargo'].value,
      sintomas: {
        tos: this.myForm.controls['tos'].value,
        fatiga: this.myForm.controls['fatiga'].value,
        fiebre: this.myForm.controls['fiebre'].value,
        resfriado: this.myForm.controls['resfriado'].value,
        escalofrios: this.myForm.controls['escalofrios'].value,
        dolorMuscular: this.myForm.controls['dolorMuscular'].value,
        dolorCabeza: this.myForm.controls['dolorCabeza'].value,
        dolorGarganta: this.myForm.controls['dolorGarganta'].value,
        perdidaOlfato: this.myForm.controls['perdidaOlfato'].value,
        contactoCovid: this.myForm.controls['contactoCovid'].value,
        contactoSintomasCovid: this.myForm.controls['contactoSintomas'].value,
        viaje: this.myForm.controls['viaje'].value,
      }
    }
    this.authService.saveReport(this.reporte);
    this.showAlert();
    this.myForm.reset();

  }

  showAlert() {
    this.alertController.create({
      header : 'Registro correcto',
      subHeader : '',
      message : 'La autoevaluación ha sido registrada correctamente.',
      buttons : ['Ok']
      })
    .then(alert => alert.present());
  }

}
