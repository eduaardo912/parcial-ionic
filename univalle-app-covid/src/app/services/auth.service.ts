import { Injectable } from '@angular/core';
import { User } from '../models/user.interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Reporte } from '../models/reporte.interface';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user$: Observable<User>;

  constructor(private afAuth:AngularFireAuth, private afs:AngularFirestore, 
    private afDB:AngularFireDatabase) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap((user) => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();  // Recuperar los datos si el usuario ya existe
        }
        return of(null);
      })
    );
  }

  async login(email: string, password: string): Promise<User> {
    try {
      const { user } = await this.afAuth.signInWithEmailAndPassword(email, password);
      this.updateUserData(user);
      return user;
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  async loginGoogle(): Promise<User> {
    try {
      const { user } = await this.afAuth.signInWithPopup(new auth.GoogleAuthProvider());
      this.updateUserData(user);
      return user;
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  async register(email:string, password:string): Promise<User> {
    try {
      const { user } = await this.afAuth.createUserWithEmailAndPassword(email, password);
      await this.sendVerificationEmail();
      return user;
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  async sendVerificationEmail(): Promise<void> {
    try {
      return (await this.afAuth.currentUser).sendEmailVerification();
    } catch (error) {
      console.log('Error login:', error);
    }
  }

  isEmailVerified(user: User): boolean {
    return user.emailVerified === true ? true : false;
  }
  
  async logout(): Promise<void> {
    try {
      await this.afAuth.signOut();
    } catch (error) {
      console.log('Error logout:', error);
    }
  }

  async resetPassword(email:string): Promise<void> {
    try {
      return this.afAuth.sendPasswordResetEmail(email);
    } catch (error) {
      console.log('Error logout:', error);
    }
  }

  private updateUserData(user: User) {
    const userRef:AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,  
      email: user.email,
      emailVerified: user.emailVerified,
      displayName: user.displayName
    };
    return userRef.set(data, { merge: true });
  }

  getUserAuth() {
    return this.afAuth.authState;
  }

  public saveReport(reporte:Reporte) {
    let key = this.afDB.list('/reports/').push(reporte).key;
        //Guardamos el reporte y obtenemos el id que firebase asigna.
        //Al guardarse sin id el reporte, ahora la actualizamos con el id que firebase nos devuelve.
        reporte.uid = key;
        this.afDB.database.ref('reports/'+reporte.uid).set(reporte);
  }

  /**
   * Ver todos los reportes existentes en la BD.
   */
  public getReports() {
    return this.afDB.list('reports/').valueChanges();
  }

}
