import { User } from './user.interface';

export interface Reporte {
    uid: string;
    fecha: Date;
    hora: Date;
    user: {
        uid: string;
        email: string;
        displayName: string;
        emailVerified: boolean;
    };
    nombre: string;
    codigo: string;
    edad: number;
    medioTransporte: string;
    cargo: string;
    sintomas: {
        tos: string,
        fatiga: string,
        fiebre: string,
        resfriado: string,
        escalofrios: string,
        dolorMuscular: string,
        dolorCabeza: string,
        dolorGarganta: string,
        perdidaOlfato: string,
        contactoCovid: string,
        contactoSintomasCovid: string,
        viaje: string,
    }
}