import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formLogin: FormGroup;

  constructor(private authService: AuthService, private router: Router, 
    public navCtrl: NavController,
    private formBuilder: FormBuilder) {
      this.formLogin = this.createMyForm();
  }

  ngOnInit() { }

  private createMyForm() {
    return this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  async onLogin() {
    try {
      const user = await this.authService.login(
        this.formLogin.controls['email'].value, 
        this.formLogin.controls['password'].value
      );
      console.log(user.uid);
      if (user) {
        // Check email
        const isVerified = this.authService.isEmailVerified(user);
        this.redirectUser(isVerified);
      }
    } catch (error) {
      console.log('Error:', error);
    }
  }

  async onLoginGoogle() {
    try {
      const user = await this.authService.loginGoogle();
      console.log(user.uid);
      if (user) {
        // Check email
        const isVerified = this.authService.isEmailVerified(user);
        this.redirectUser(isVerified);
      }
    } catch (error) {
      console.log('Error:', error);
    }
  }

  private redirectUser(isVerified: boolean): void {
    // Si es un usuario verificado o que accede con Google, 
    // redireccionar al home de la app.
    this.formLogin.reset();
    if (isVerified) {
      this.router.navigate(['/tabs/tab1']);
    } else {
      this.router.navigate(['verify-email']);
    }
  }

}
