import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

import { AngularFireDatabase } from '@angular/fire/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  formRegister: FormGroup;

  constructor(private authService: AuthService, 
    private router: Router, 
    private afDB: AngularFireDatabase,
    public navCtrl: NavController,
    private formBuilder: FormBuilder) {
      this.formRegister = this.createMyForm();
    }

  ngOnInit() { }

  private createMyForm() {
    return this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  async onRegister() {
    try {
      const user = await this.authService.register(
        this.formRegister.controls['email'].value, 
        this.formRegister.controls['password'].value
      );
      if (user) {
        console.log('User:', user);
        const isVerified = this.authService.isEmailVerified(user);
        this.redirectUser(isVerified);

        this.afDB.object('users/'+user.uid).set({
          email: user.email,
          displayName: user.displayName,
          emailVerified: user.emailVerified
        });

        // Check email
      }
    } catch (error) {
      console.log('Error:', error);
    }
  }

  private redirectUser(isVerified: boolean): void {
    // Si es un usuario verificado o que accede con Google, 
    // redireccionar al home de la app.
    this.formRegister.reset();
    if (isVerified) {
      this.router.navigate(['/tabs/tab1']);
    } else {
      this.router.navigate(['verify-email']);
    }
  }
  
}