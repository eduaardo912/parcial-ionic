import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  text: any;

  constructor(private barcodeScanner: BarcodeScanner,
    private alertController: AlertController) { }

  scanCode() {
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.text = barcodeData.text;
      }
    );
  }

}
