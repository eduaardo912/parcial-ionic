// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
  firebaseConfig: {
    apiKey: "AIzaSyDWztkQgjTvHqm0ZR3IsFrGhrkmVyf2RqM",
    authDomain: "univalle-appcovid.firebaseapp.com",
    databaseURL: "https://univalle-appcovid.firebaseio.com",
    projectId: "univalle-appcovid",
    storageBucket: "univalle-appcovid.appspot.com",
    messagingSenderId: "636814333966",
    appId: "1:636814333966:web:a4fa04bacfbc3a4e59845f",
    measurementId: "G-HJHMSP1YXW"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
